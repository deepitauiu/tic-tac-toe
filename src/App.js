import React, {Component} from 'react';
import './App.css';
import Status from './Status'

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            board: Array(9).fill(null),
            player: null,
            winner: null
        }
        // this.state.checkwinner=this.state.checkwinner.bind(this);
    }

    checkwinner() {
        let winLines =
            [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8],
                [0, 3, 6],
                [1, 4, 7],
                [2, 5, 8],
                [0, 4, 8],
                [2, 4, 6],
            ]
        for (let index = 0; index < winLines.length; index++) {
            const [a, b, c] = winLines[index];
            if (this.state.board[a] && this.state.board[a] === this.state.board[b]
                && this.state.board[a] === this.state.board[c]) {
                this.setState({
                    winner: this.state.player
                });
            }
        }
    }

    handleClick(index) {
        if (this.state.player && !this.state.winner) {
            let newBoard = this.state.board;
            if (newBoard[index] === null) {
                newBoard[index] = this.state.player;
                this.setState({
                    board: newBoard,
                    player: this.state.player === "x" ? "0" : "x"
                });
            }
            this.checkwinner();
        }
    }

    setPlayer(e) {
        this.setState({
            player: e
        });

    }

    render() {

        const Box = this.state.board.map((box, index) =>
            <div className="box"
                 key={index}
                 onClick={() => this.handleClick(index)}>
                {box}
            </div>);
        return (
            <div className="App">
                <h1>Tic Tac Toe</h1>
                <Status player={this.state.player} winner={this.state.winner} setPlayer={(e)=>{this.setPlayer(e)}}/>
                <div className="board">
                    {Box}
                </div>
            </div>
        );
    }
}

export default App;
