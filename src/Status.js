import React, {Component} from 'react';
import Player from './choosePlayer'

class Status extends Component {
    handleSetPlayer(e){
        this.props.setPlayer(e);
    }
    render(){
        return(
            this.props.player
                ? this.props.winner
                ? <h2>Player {this.props.winner} win this match</h2>
                : <h2>Next Player is {this.props.player}</h2>
                : <Player player={(e) => this.handleSetPlayer(e)}/>
        )
    }
}

export default Status;