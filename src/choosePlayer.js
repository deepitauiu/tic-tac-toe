import React, {Component} from 'react';

class Player extends Component {
    handleFrom(e){
        e.preventDefault();
        this.props.player(e.target.player.value);
    }

    render(){
        return(
            <form onSubmit={(e)=>this.handleFrom(e)}>
                <label>
                    Player x
                    <input type="radio" name="player" value="x"/>
                </label>
                <label>
                    Player 0
                    <input type="radio" name="player" value="0"/>
                </label>
                <input type="submit" value="start"/>

            </form>
        );


    }
}
export default Player;